package de.born.arquillian;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import junit.framework.Assert;

@RunWith(Arquillian.class)
public class MitarbeiterIT {
  
  @Deployment
  public static Archive createDeployment() {
    WebArchive webArchive = ShrinkWrap.create(WebArchive.class);
    webArchive.addPackages(true, "de.born.arquillian");
    return webArchive;
  }
  
  @Inject
  private Mitarbeiter mitarbeiter;
  
  @Test
  public void nameTest() {
    Assert.assertEquals("Mustermann1", this.mitarbeiter.name);
  }
  
  @Test
  public void vNameTest() {
    Assert.assertEquals("Max", this.mitarbeiter.vName);
  }
  
}
